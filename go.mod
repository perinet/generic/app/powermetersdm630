module main

go 1.18

require (
	gitlab.com/perinet/generic/apiservice/dnssd v0.0.0-20230303101032-12faee2d9b44
	gitlab.com/perinet/generic/apiservice/mqttclient v0.0.0-20230303115153-48a114e2dc87
	gitlab.com/perinet/generic/apiservice/powermetersdm630 v0.0.0-20230303123521-878cf4aee8ba
	gitlab.com/perinet/generic/apiservice/staticfiles v0.0.0-20221007144050-1a8dc2782f6a
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20221122163058-59cb209fd3f2
	gitlab.com/perinet/generic/lib/utils v0.0.0-20221122162821-91b714770155
	gitlab.com/perinet/periMICA-container/apiservice/lifecycle v0.0.0-20221101161231-d517bb9ab0d3
	gitlab.com/perinet/periMICA-container/apiservice/node v0.0.0-20221130091942-a456b86bb5af
	gitlab.com/perinet/periMICA-container/apiservice/security v0.0.0-20230210151025-9f2dd56cd345
)

require (
	actshad.dev/modbus v0.2.1 // indirect
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/eclipse/paho.mqtt.golang v1.4.2 // indirect
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/holoplot/go-avahi v1.0.1 // indirect
	golang.org/x/exp v0.0.0-20230202163644-54bba9f4231b // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
)
