/*
 * Copyright (c) 2018-2022 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Packagepowermetersdm630 implements the Powermeter SDM-630 application
// part of the unified api.
package main

import (
	"encoding/json"
	"flag"
	"log"
	"time"

	"gitlab.com/perinet/generic/apiservice/dnssd"
	mqtt "gitlab.com/perinet/generic/apiservice/mqttclient"
	sdm630 "gitlab.com/perinet/generic/apiservice/powermetersdm630"
	static "gitlab.com/perinet/generic/apiservice/staticfiles"
	httpserver "gitlab.com/perinet/generic/lib/httpserver"
	auth "gitlab.com/perinet/generic/lib/httpserver/auth"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
	lifecycle "gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	node "gitlab.com/perinet/periMICA-container/apiservice/node"
	security "gitlab.com/perinet/periMICA-container/apiservice/security"
)

var (
	nodeInfo          node.NodeInfo
	productionInfo    node.ProductionInfo
	securityConfig    security.SecurityConfig
	Logger            log.Logger = *log.Default()
	publisherEndpoint mqtt.MQTTClientPublisherEndpoint
	powermeterConfig  sdm630.PowermeterSDM630Config
)

func init() {
	Logger.SetPrefix("SDM630: ")
	Logger.Println("Starting")

	var data []byte
	// update apiservice node with used services
	services := []string{"node", "security", "lifecycle", "dns-sd"}
	data, _ = json.Marshal(services)
	webhelper.InternalPut(node.ServicesSet, data)

	var err error
	data = webhelper.InternalGet(node.NodeInfoGet)
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		Logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(node.ProductionInfoGet), &productionInfo)
	if err != nil {
		Logger.Println("Failed to fetch ProductionInfo: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(security.Security_Config_Get), &securityConfig)
	if err != nil {
		Logger.Println("Failed to fetch SecurityConfig: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(sdm630.PowermeterSDM630Config_Get), &powermeterConfig)
	if err != nil {
		Logger.Println("Failed to fetch PowermeterSdm630Config: ", err.Error())
	}

	// start advertising via dnssd
	dnssdServiceInfo := dnssd.DNSSDServiceInfo{ServiceName: "_https._tcp", Port: 443}
	dnssdServiceInfo.TxtRecord = []string{}
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "api_version="+nodeInfo.ApiVersion)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "life_state="+nodeInfo.LifeState)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "application_name="+nodeInfo.Config.ApplicationName)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "element_name="+nodeInfo.Config.ElementName)
	data, _ = json.Marshal(dnssdServiceInfo)
	webhelper.InternalVarsPut(dnssd.DNSSDServiceInfoAdvertiseSet, map[string]string{"service_name": dnssdServiceInfo.ServiceName}, data)

	topic := nodeInfo.Config.ApplicationName + "/" + nodeInfo.Config.ElementName
	publisherEndpoint = mqtt.NewPublisherEndpoint(topic)
	publisherEndpoint.Topic = nodeInfo.Config.ApplicationName + "/" + nodeInfo.Config.ElementName

	go publish()
}

func publish() {
	for { // ever
		// get sample values from powermeter api service
		res := webhelper.InternalGet(sdm630.PowermeterSDM630Sample_Get)
		msg := string(res[:])

		publisherEndpoint.Message <- msg

		time.Sleep(time.Millisecond * time.Duration(powermeterConfig.SampleRate*1000))
	}
}

func main() {
	var webui_path string
	flag.StringVar(&webui_path, "u", "./webui", "Specify path to serve the web ui. Default is ./webui")
	flag.Parse()

	static.Set_files_path(webui_path)

	httpserver.AddPaths(static.PathsGet())
	httpserver.AddPaths(dnssd.PathsGet())
	httpserver.AddPaths(sdm630.PathsGet())
	httpserver.AddPaths(node.PathsGet())
	httpserver.AddPaths(mqtt.PathsGet())
	httpserver.AddPaths(lifecycle.PathsGet())

	auth.SetAuthMethod(securityConfig.ClientAuthMethod)

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      webhelper.InternalVarsGet(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    webhelper.InternalVarsGet(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: webhelper.InternalVarsGet(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})

	httpserver.ListenAndServe("[::]:443")
}
